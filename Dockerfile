FROM node:14.19.0-alpine3.15

RUN apk update && apk upgrade && apk --no-cache add curl alpine-sdk bash libstdc++ libc6-compat
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools

RUN npm config set python python3

RUN curl -fsSL https://code-server.dev/install.sh | sh

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 80